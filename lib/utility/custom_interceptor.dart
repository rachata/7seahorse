
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:seahorse/alert/alert_basic.dart';
import 'package:seahorse/auth/login_page.dart';
import 'package:seahorse/main.dart';
import 'package:seahorse/model/user.dart';
import 'package:seahorse/utility/config.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';

class CustomInterceptor extends Interceptor{

  late Dio dio;
  late  BuildContext context ;
  ProgressDialog? pd;

  late bool status;

  CustomInterceptor(dio , context , {status = true} ){
    this.status = status;
    this.dio = dio;
    this.context = context;


    if(status){
      pd = ProgressDialog(context: context);
    }


  }
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler)async{

    String _userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.59";

    if(options.uri.path != "/emp/login"){

      UserInfo userInfo =  UserInfo();
      LoginUser? user = await userInfo.getUserInfo();

      options.headers["Authorization"] = "${user!.token}";
    }

    options.headers["User-Agent"] = _userAgent;


    logger.d("HTTP Request ${options.headers.toString()}");

    if(this.status  ) pd!.show(max: 100, msg: 'please wait..');
    handler.next(options);

  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async{

    logger.e("HTTP Error ${err.requestOptions.uri.toString()} ${err.response?.statusCode}");



    if (err.response?.statusCode == 401) {

      dio.lock();


      RequestOptions options = err.response!.requestOptions;

      try {

        UserInfo userInfo =  UserInfo();
        LoginUser? user = await userInfo.getUserInfo();

        Map<String, dynamic> headers = Map<String, dynamic>();

        Response res = await Dio()
            .get("${ConfigServer.serverHTTP}/emp/login" ,
            options: Options(headers: {
              "Authorization" : user?.token ,
              "refreshToken" : user?.refreshToken,
              "User-Agent" : "${options.headers["User-Agent"]}"
            } ));



        user?.refreshToken = res.data["refreshToken"];
        user?.token = res.data["token"];


        final body = json.encode(user?.toJson());

        userInfo.setUserInfo(body);

        headers["Authorization"] = "${res.data["token"]}";
        headers["refreshToken"] = "${res.data["refreshToken"]}";
        headers["User-Agent"] = options.headers["User-Agent"];



        dio.unlock();
        dio.interceptors.errorLock.unlock();
        dio.interceptors.requestLock.unlock();
        dio.interceptors.responseLock.unlock();


        Response response = await dio.request(options.path,
            cancelToken: options.cancelToken,
            data : options.data,
            queryParameters: options.queryParameters,
            onReceiveProgress: options.onReceiveProgress,
            onSendProgress: options.onSendProgress,
            options: Options(headers: headers , method:  err.requestOptions.method));

        handler.resolve(response);

      } on DioError catch (e) {


        logger.e("HTTP Error Refresh Token ${e.requestOptions.uri.toString()} ${e.message} ${e.response!.data}");


        UserInfo().logout();

        if(this.status ) pd!.close();

        await showAlertGoToLogin("กรุณาเข้าสู่ระบบอีกครั้ง");

        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (_) => LoginPage()));


      }

    } else if (err.response?.statusCode == 403) {


      UserInfo().logout();
      if(this.status ) pd!.close();

      await showAlertGoToLogin("กรุณาเข้าสู่ระบบอีกครั้ง");

      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_) => LoginPage()));

    } else {
      if(this.status )     pd!.close();
      await showAlertGoToLogin("กรุณาลองใหม่อีกครั้ง ${err.error}");
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if(this.status )   pd!.close();


    logger.d("HTTP Response ${response.requestOptions.uri.toString()} ${response.statusCode} ${response.data}");

    handler.next(response);

  }
  Future showAlertGoToLogin(String detail)async{

    print(detail);
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertInfo(
            title: "ผิดพลาด",
            detail: "${detail}",
          );
        });

  }
}