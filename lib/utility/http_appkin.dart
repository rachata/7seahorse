
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import 'config.dart';
import 'custom_interceptor.dart';

class HTTPAppKin {
  late Dio _dio;
  late BuildContext _context;
  late bool _status;

  HTTPAppKin(BuildContext context , {bool status = true})  {

    _dio = Dio();
    this._status = status;
    _context = context;

    _dio.interceptors.clear();
    _dio.interceptors.add(CustomInterceptor(_dio, _context , status: this._status ));
    _dio.options.baseUrl = ConfigServer.serverHTTP;


  }
  Future<Response> get(String path,
      {Map<String, dynamic>? queryParameters, Options? options}) async {
    Response response = await _dio.get(path,
        queryParameters: queryParameters, options: options);
    return response;
  }

  Future<Response> post(
      String path, {
        data  , Map<String, dynamic>? queryParameters , Options? options,
      }) async {
    Response response = await _dio.post(path,
        queryParameters: queryParameters, options: options, data: data);
    return response;
  }
}


class HTTPAppKinMain {
  late Dio _dio;
  late BuildContext _context;
  late bool _status;

  HTTPAppKinMain(BuildContext context , {bool status = true})  {

    _dio = Dio();
    this._status = status;
    _context = context;

    _dio.interceptors.clear();
    _dio.interceptors.add(CustomInterceptor(_dio, _context , status: this._status ));
    _dio.options.baseUrl = ConfigServer.serverHTTPMain;


  }
  Future<Response> get(String path,
      {Map<String, dynamic>? queryParameters, Options? options}) async {
    Response response = await _dio.get(path,
        queryParameters: queryParameters, options: options);
    return response;
  }

  Future<Response> post(
      String path, {
        data  , Map<String, dynamic>? queryParameters , Options? options,
      }) async {
    Response response = await _dio.post(path,
        queryParameters: queryParameters, options: options, data: data);
    return response;
  }
}

