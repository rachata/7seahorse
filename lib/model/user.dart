import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class LoginUser {
  late bool success;
  late String token;
  late String refreshToken;
  late User user;

  LoginUser({required this.success,required this.token, required this.refreshToken, required this.user});

  LoginUser.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    token = json['token'];
    refreshToken = json['refreshToken'];
    user = (json['user'] != null ? new User.fromJson(json['user']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['token'] = this.token;
    data['refreshToken'] = this.refreshToken;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  late String id;
  late int empid;
  late String name;
  late String username;
  late String lastname;
  late String shopid;
  late String shopnameTh;
  late String shopType;
  late  Null profile;

  User (
      {required this.id,
        required this.empid,
        required this.name,
        required  this.username,
        required  this.lastname,
        required this.shopid,
        required  this.shopnameTh,
        required  this.shopType,
        required  this.profile});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    empid = json['empid'];
    name = json['name'];
    username = json['username'];
    lastname = json['lastname'];
    shopid = json['shopid'];
    shopnameTh = json['shopnameTh'];
    shopType = json['shopType'];
    profile = json['profile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['empid'] = this.empid;
    data['name'] = this.name;
    data['username'] = this.username;
    data['lastname'] = this.lastname;
    data['shopid'] = this.shopid;
    data['shopnameTh'] = this.shopnameTh;
    data['shopType'] = this.shopType;
    data['profile'] = this.profile;
    return data;
  }
}

class UserInfo{
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<LoginUser?> getUserInfo()async{

    final SharedPreferences prefs = await _prefs;

    String? jwt = prefs.getString("user");

    if (jwt == null || jwt == "") {
      return null;

    }else{
      final jsonPrefs = json.decode(jwt);

      LoginUser loginUser = LoginUser.fromJson(jsonPrefs);
      return loginUser;
    }
  }


  Future<void> logout()async{

    final SharedPreferences prefs = await _prefs;

    prefs.setString("user", "");
    logger.i("User Logout");
  }

  Future<bool> setUserInfo(String data)async{

    print("[setUserInfo] ${data}");

    final SharedPreferences prefs = await _prefs;
    await prefs.setString("user", data);

    return true;
  }

}