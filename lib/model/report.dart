class ReportModel {
  bool? status;
  String? message;
  String? title;
  List<Data>? data;
  int? amount;
  int? total;

  ReportModel(
      {this.status,
        this.message,
        this.title,
        this.data,
        this.amount,
        this.total});

  ReportModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    title = json['title'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    amount = json['amount'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['title'] = this.title;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['amount'] = this.amount;
    data['total'] = this.total;
    return data;
  }
}

class Data {
  int? iId;
  String? title;
  int? total;
  int? amount;

  Data({this.iId, this.title, this.total, this.amount});

  Data.fromJson(Map<String, dynamic> json) {
    iId = json['_id'];
    title = json['title'];
    total = json['total'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.iId;
    data['title'] = this.title;
    data['total'] = this.total;
    data['amount'] = this.amount;
    return data;
  }
}