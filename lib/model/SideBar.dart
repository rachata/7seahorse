class SideBarModel {
  int action;

  SideBarModel({required this.action});

  @override
  String toString() {
    return 'SideBarModel {id: $action}';
  }

}

