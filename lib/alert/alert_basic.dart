
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertInfo extends StatefulWidget {
  final String title;
  final String detail;
  final bool status;


  const AlertInfo({required this.title, required this.detail , this.status = false});

  @override
  _AlertInfotate createState() => _AlertInfotate();
}

class _AlertInfotate extends State<AlertInfo> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.only(top: 10.0),
      content: Container(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Stack(
                  overflow: Overflow.visible,
                  alignment: Alignment.topCenter,
                  children: [
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 70, 10, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '${widget.title}',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              '${widget.detail}',
                              style: TextStyle(fontSize: 20),
                            ),
                            SizedBox(
                              height: 7,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [

                                GestureDetector(
                                  onTap: () => {Navigator.pop(context, true)},
                                  child: Container(
                                    padding: EdgeInsets.all(10.0),
                                    decoration: BoxDecoration(
                                      color: widget.status == true ? Colors.green :Colors.redAccent,
                                      border: Border.all(
                                          width: 1.0, color: widget.status == true ? Colors.green  : Colors.redAccent),
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: Text(
                                      'ตกลง',
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                        top: -60,
                        child: CircleAvatar(
                            backgroundColor: widget.status == true ? Colors.green : Colors.redAccent,
                            radius: 60,
                            child: Text(
                              "${widget.status == true ? "OK" : "!"}",
                              style:
                              TextStyle(color: Colors.white, fontSize: 50),
                            ))),
                  ],
                )
              ],
            ),
          )),
    );
  }
}



class AlertYesNo extends StatefulWidget {
  final String title;
  final String detail;



  const AlertYesNo({required this.title, required this.detail});

  @override
  _AlertYesNotate createState() => _AlertYesNotate();
}

class _AlertYesNotate extends State<AlertYesNo> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.only(top: 10.0),
      content: Container(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Stack(
                  overflow: Overflow.visible,
                  alignment: Alignment.topCenter,
                  children: [
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 70, 10, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '${widget.title}',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              '${widget.detail}',
                              style: TextStyle(fontSize: 20),
                            ),
                            SizedBox(
                              height: 7,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [

                                GestureDetector(
                                  onTap: () => {Navigator.pop(context, true)},
                                  child: Container(
                                    padding: EdgeInsets.all(10.0),
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      border: Border.all(
                                          width: 1.0, color:  Colors.green ),
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: Text(
                                      'ตกลง',
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.white),
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () => {Navigator.pop(context, false)},
                                  child: Container(
                                    padding: EdgeInsets.all(10.0),
                                    decoration: BoxDecoration(
                                      color: Colors.redAccent,
                                      border: Border.all(
                                          width: 1.0, color:  Colors.redAccent ),
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: Text(
                                      'ยกเลิก',
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                        top: -60,
                        child: CircleAvatar(
                            backgroundColor: Colors.redAccent,
                            radius: 60,
                            child: Text(
                              "?",
                              style:
                              TextStyle(color: Colors.white, fontSize: 50),
                            ))),
                  ],
                )
              ],
            ),
          )),
    );
  }
}
