import 'package:flutter/material.dart';
import 'package:seahorse/main.dart';
import 'package:seahorse/utility/app_theme.dart';


class AlertAlert extends StatefulWidget {

   final dynamic data;

  const AlertAlert({Key? key, this.data}) : super(key: key);



  @override
  _AlertAlerttate createState() => _AlertAlerttate();
}

class _AlertAlerttate extends State<AlertAlert> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.only(top: 10.0),
      content: Container(
          height: MediaQuery
              .of(context)
              .size
              .height * 0.2,
          width: MediaQuery
              .of(context)
              .size
              .width * 0.3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,

            children: <Widget>[
              Container(
                child: Text(
                  'สรุปบิล',
                  style: TextStyle(
                      color: Color(0xFF666666),
                      fontWeight: FontWeight.w600,
                      fontSize: 24),
                ),
              ),

              Padding(padding: EdgeInsets.only(left: 10, right: 10),
                child: Divider(
                  thickness: 1,
                  color: AppTheme.SEAHORSE_PRIMARY.withOpacity(0.8),),)
              ,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(padding: EdgeInsets.only(left: 10), child: Text(
                    'รับเงิน',
                    style: TextStyle(
                        color: Color(0xFF666666),
                        fontWeight: FontWeight.w600,
                        fontSize: 20),
                  ) ,),

                  Padding(padding: EdgeInsets.only(right: 10),
                    child: Row(

                      children: [
                        Text(
                          "${numberFormat.format(widget.data['cash'])}",
                          style: TextStyle(
                              color: Color(0xFF5D6AE0),
                              fontWeight: FontWeight.w600,
                              fontSize: 20),
                        ),
                        Text(
                          '฿',
                          style: TextStyle(
                              color: Color(0xFF666666),
                              fontWeight: FontWeight.w600,
                              fontSize: 24),
                        )
                      ],
                    ) ,)

                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(padding: EdgeInsets.only(left: 10), child: Text(
                    'เงินทอน',
                    style: TextStyle(
                        color: Color(0xFF666666),
                        fontWeight: FontWeight.w600,
                        fontSize: 24),
                  ) ,),

                  Padding(padding: EdgeInsets.only(right: 10),
                    child: Row(

                      children: [
                        Text(
                          "${numberFormat.format(widget.data['change'])}",
                          style: TextStyle(
                              color: Color(0xFFFF5858),
                              fontWeight: FontWeight.w600,
                              fontSize: 24),
                        ),
                        Text(
                          '฿',
                          style: TextStyle(
                              color: Color(0xFFFF5858),
                              fontWeight: FontWeight.w600,
                              fontSize: 24),
                        )
                      ],
                    ) ,)

                ],
              ),
              Container(
                width: 160,
                height: 40,

                decoration: BoxDecoration(
                  color: Color(0xFF5D6AE0),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10.0),
                      bottomRight:
                      Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                      bottomLeft:
                      Radius.circular(10.0)),
                ),
                child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0),
                        side: BorderSide(
                            color: Colors.white,
                            width: 1,
                            style: BorderStyle.solid
                        )
                    ),

                    onPressed: () => {
                      Navigator.pop(context)

                    },
                    child: Text(
                      "เสร็จสิ้น",
                      style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.normal,
                          color: Colors.white
                      ),
                    )
                ),
              )
            ],
          )),
    );
  }
}


