import 'package:flutter/material.dart';


class AlertWaiting extends StatefulWidget {



  @override
  _AlertWaitingtate createState() => _AlertWaitingtate();
}

class _AlertWaitingtate extends State<AlertWaiting> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.only(top: 10.0),
      content: Container(
        height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width * 0.3,
          child:  Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,

            children: <Widget>[
              Container(
                child: Text(
                  'รอสักครู่ ...',
                  style: TextStyle(
                    color : Color(0xFF666666),
                  fontWeight: FontWeight.w600,
                      fontSize: 24),
                ),
              ),
            ],
          )),
    );
  }
}


