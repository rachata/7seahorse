import 'package:flutter/material.dart';
import 'package:seahorse/model/SideBar.dart';
import 'package:seahorse/utility/app_theme.dart';

import 'main.dart';

class SideBar extends StatefulWidget {
  const SideBar({Key? key}) : super(key: key);

  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  Color select = Color(0xFF5D6AE0);
  Color unSelect = Color(0xFFFFFFFF);

  int status = 1;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: 280,
      height: MediaQuery.of(context).size.height - 74,
      child: ListView(
        children: [
          Container(
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: status == 1 ? select.withOpacity(0.2) :  unSelect.withOpacity(0.2),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                  topLeft: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0)),
            ),
            child: ListTile(
              onTap: ()=>{

                status = 1,
                eventBus.fire(SideBarModel(action: 1))
              },
              leading: IconButton(
                icon: new Image.asset(
                  'assets/images/archive.png',
                  color:  status == 1 ?  AppTheme.SEAHORSE_PRIMARY : Color(0xFF6A6D82),
                ),
                tooltip: 'Closes application',
                onPressed: () => { status = 1,eventBus.fire(SideBarModel(action: 1))},
              ),
              title: Text(
                "หน้าแรก",
                style: TextStyle(fontSize: 20, color: status == 1 ?  select :  Color(0xFF6A6D82)),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: status == 2 ? select.withOpacity(0.2) :  unSelect.withOpacity(0.2),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                  topLeft: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0)),
            ),
            child: ListTile(
              onTap: ()=>{
                status = 2,
                eventBus.fire(SideBarModel(action: 2))
              },
              leading: IconButton(
                icon: new Image.asset(
                  'assets/images/briefcase.png',
                  color:  status == 2 ?  AppTheme.SEAHORSE_PRIMARY : Color(0xFF6A6D82),
                ),
                tooltip: 'Closes application',
                onPressed: () => { status = 2,eventBus.fire(SideBarModel(action: 2))},
              ),
              title: Text(
                "รายงาน",
                style: TextStyle(fontSize: 20, color: status == 2 ?  select :  Color(0xFF6A6D82)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
