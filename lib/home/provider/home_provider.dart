import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:seahorse/utility/config.dart';
import 'package:seahorse/utility/http_appkin.dart';

class HomeProvider extends ChangeNotifier {

  Future<Response> cashierCreate(BuildContext context, data) async {
    Response response = await HTTPAppKinMain(context)
        .post("${ConfigServer.serverHTTPMain}/ticket/create", data: data);
    return response;
  }

  Future<Response> getConfig(BuildContext context) async {
    Response response = await HTTPAppKinMain(context)
        .get("${ConfigServer.serverHTTPMain}/ticket/config");
    return response;
  }



}