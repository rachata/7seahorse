import 'dart:convert';

import 'package:after_layout/after_layout.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seahorse/alert/alert_basic.dart';
import 'package:seahorse/alert/alert_money.dart';
import 'package:seahorse/alert/alert_wait.dart';
import 'package:seahorse/home/provider/home_provider.dart';
import 'package:seahorse/model/report.dart';
import 'package:seahorse/report/provider/report_provider.dart';
import 'package:seahorse/utility/app_theme.dart';
import 'package:url_launcher/url_launcher.dart';

import '../main.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with AfterLayoutMixin<HomePage> {
  int numberOfPeoples = 1;
  int pricePer1Use = 1;

  String strMoneyInCome = "0";

  TextEditingController moneyInCome = TextEditingController(text: "");
  FocusNode _focusMoneyInCome = FocusNode();

  TextEditingController peoples = TextEditingController(text: "");
  FocusNode _focusPeoples = FocusNode();

  int peoplesCount = 0;


  bool isPeople = false;
  bool isMoneyInCome = false;

  @override
  void initState() {
    super.initState();
    _focusMoneyInCome.addListener(_onFocusChange);

    _focusPeoples.addListener(_onFocusChangePeoples);
  }

  @override
  void dispose() {
    super.dispose();
    _focusMoneyInCome.removeListener(_onFocusChange);
    _focusMoneyInCome.dispose();

    _focusPeoples.removeListener(_onFocusChange);
    _focusPeoples.dispose();
  }

  void _onFocusChange() {
    debugPrint("_focusMoneyInCome: ${_focusMoneyInCome.hasFocus.toString()}");
  }

  void _onFocusChangePeoples() {
    debugPrint("_focusPeoples: ${_focusPeoples.hasFocus.toString()}");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 280,
      height: MediaQuery.of(context).size.height - 74,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                width: MediaQuery.of(context).size.width - 280,
                height: 55,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(10.0)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width - 500,
                      child: Row(
                        children: [
                          IconButton(
                            icon: new Image.asset(
                              'assets/images/announce.png',
                              color: AppTheme.SEAHORSE_PRIMARY,
                            ),
                            tooltip: 'Closes application',
                            onPressed: () => {},
                          ),

                          Flexible(
                            child: RichText(
                              overflow: TextOverflow.ellipsis,
                              strutStyle: StrutStyle(fontSize: 12.0),
                              text: TextSpan(
                                  style: TextStyle(
                                      color: Color(0xFF6A6D82), fontSize: 18),
                                  text: 'ประกาศ'),
                            ),
                          ),

                          // Text(
                          //   "\ewfewfwef",
                          //   style:
                          //       TextStyle(color: Color(0xFF6A6D82), fontSize: 18),
                          // ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () => {_launchURL("www.google.com")},
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: VerticalDivider(
                              color: AppTheme.SEAHORSE_GREY_70.withOpacity(0.1),
                              width: 1.5,
                              thickness: 1.5,
                            ),
                          ),
                          Text(
                            "More",
                            style: TextStyle(
                                color: Color(0xFF6A6D82), fontSize: 18),
                          ),
                          Icon(
                            Icons.arrow_forward_ios_sharp,
                            color: Color(0xFF6A6D82),
                          ),
                        ],
                      ),
                    )
                  ],
                )),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: ((MediaQuery.of(context).size.width - 280) * 0.8) - 20,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0)),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "ระบบขาย",
                        style: TextStyle(
                            fontSize: 24,
                            color: Color(0xFF666666),
                            fontWeight: FontWeight.w600),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            color: Colors.white,
                            width: (((MediaQuery.of(context).size.width - 280) *
                                        0.9) -
                                    40) *
                                0.5,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    GestureDetector(
                                      onTap: () => {numberOfPeople(false)},
                                      child: Container(
                                          width: 90,
                                          height: 90,
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            color: Color(0xFF5D6AE0),
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(10.0),
                                                bottomRight:
                                                    Radius.circular(10.0),
                                                topLeft: Radius.circular(10.0),
                                                bottomLeft:
                                                    Radius.circular(10.0)),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.arrow_back_ios,
                                                color: Colors.white,
                                                size: 60,
                                              )
                                            ],
                                          )),
                                    ),
                                    Container(
                                        width: 300,
                                        height: 130,
                                        padding: EdgeInsets.only(
                                            left: 10, right: 10),
                                        margin: EdgeInsets.only(
                                            left: 10, right: 10),
                                        decoration: BoxDecoration(
                                          color: Color(0xFFFFFFFF),
                                          border: Border.all(
                                              color: Color(0xFFB9B9B9)),
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(10.0),
                                              bottomRight:
                                                  Radius.circular(10.0),
                                              topLeft: Radius.circular(10.0),
                                              bottomLeft:
                                                  Radius.circular(10.0)),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "จำนวน",
                                              style: TextStyle(
                                                  color: Color(0xFFB9B9B9),
                                                  fontSize: 18),
                                            ),
                                            Container(
                                                width: 150,
                                                padding: EdgeInsets.all(0),
                                                margin: EdgeInsets.all(0),
                                                child: Theme(
                                                  data: new ThemeData(
                                                    primaryColor:
                                                        Colors.redAccent,
                                                    primaryColorDark:
                                                        Colors.red,
                                                  ),
                                                  child: new TextField(
                                                    keyboardType:
                                                        TextInputType.number,
                                                    controller: peoples,
                                                    textAlign: TextAlign.center,
                                                    focusNode: _focusPeoples,
                                                    onChanged: (text) =>
                                                        {checkNumber(text)},
                                                    style: TextStyle(
                                                        fontFamily:
                                                            AppTheme.fontName,
                                                        color:
                                                            Color(0xFF333333),
                                                        fontSize: 20),
                                                    decoration:
                                                        new InputDecoration(
                                                      focusedBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    10.0)),
                                                        borderSide: BorderSide(
                                                            color: AppTheme
                                                                .SEAHORSE_PRIMARY,
                                                            width: 2.0),
                                                      ),
                                                      border: new OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          10.0)),
                                                          borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .teal)),
                                                    ),
                                                  ),
                                                ))
                                          ],
                                        )),
                                    GestureDetector(
                                      onTap: () => {numberOfPeople(true)},
                                      child: Container(
                                          width: 90,
                                          height: 90,
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            color: Color(0xFF5D6AE0),
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(10.0),
                                                bottomRight:
                                                    Radius.circular(10.0),
                                                topLeft: Radius.circular(10.0),
                                                bottomLeft:
                                                    Radius.circular(10.0)),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.arrow_forward_ios,
                                                color: Colors.white,
                                                size: 60,
                                              )
                                            ],
                                          )),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      "ยอดรวม",
                                      style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF5D6AE0)),
                                    )
                                  ],
                                ),
                                Text(
                                  "${numberFormat.format(pricePer1Use * numberOfPeoples)} ฿",
                                  style: TextStyle(
                                      color: Color(0xFF0BD93B),
                                      fontSize: 28,
                                      fontWeight: FontWeight.w600),
                                ),
                                Divider(
                                  thickness: 1,
                                  color: Colors.grey.withOpacity(0.4),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "รับเงิน",
                                    style: TextStyle(
                                        color: Color(0xFF5D6AE0), fontSize: 24),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                    child: Theme(
                                  data: new ThemeData(
                                    primaryColor: Colors.redAccent,
                                    primaryColorDark: Colors.red,
                                  ),
                                  child: new TextField(
                                    focusNode: _focusMoneyInCome,
                                    controller: moneyInCome,
                                    style: TextStyle(
                                        fontFamily: AppTheme.fontName,
                                        color: Color(0xFF333333),
                                        fontSize: 24),
                                    decoration: new InputDecoration(
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10.0)),
                                          borderSide: BorderSide(
                                              color: AppTheme.SEAHORSE_PRIMARY,
                                              width: 2.0),
                                        ),
                                        border: new OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.0)),
                                            borderSide: new BorderSide(
                                                color: Colors.teal)),
                                        prefixText: ' ',
                                        suffixText: 'บาท',
                                        suffixStyle: const TextStyle(
                                            color: Color(0xFF333333),
                                            fontSize: 24)),
                                  ),
                                )),
                                SizedBox(
                                  height: 20,
                                ),
                                Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: ((((MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              280) *
                                                          0.9) -
                                                      40) *
                                                  0.5) *
                                              0.7,
                                          child: Table(
                                            children: [
                                              TableRow(children: [
                                                buildButton(
                                                    "7", 1, Colors.black54),
                                                buildButton(
                                                    "8", 1, Colors.black54),
                                                buildButton(
                                                    "9", 1, Colors.black54),
                                              ]),
                                              TableRow(children: [
                                                buildButton(
                                                    "4", 1, Colors.black54),
                                                buildButton(
                                                    "5", 1, Colors.black54),
                                                buildButton(
                                                    "6", 1, Colors.black54),
                                              ]),
                                              TableRow(children: [
                                                buildButton(
                                                    "1", 1, Colors.black54),
                                                buildButton(
                                                    "2", 1, Colors.black54),
                                                buildButton(
                                                    "3", 1, Colors.black54),
                                              ]),
                                              TableRow(children: [
                                                buildButton(
                                                    "000", 1, Colors.black54),
                                                buildButton(
                                                    "0", 1, Colors.black54),
                                                buildButton(
                                                    "00", 1, Colors.black54),
                                              ]),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          width: ((((MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              280) *
                                                          0.9) -
                                                      40) *
                                                  0.5) *
                                              0.3,
                                          child: Table(
                                            children: [
                                              TableRow(children: [
                                                buildButtonDelete(),
                                              ]),
                                              TableRow(children: [
                                                buildButton(
                                                    ".", 1, Colors.blue),
                                              ]),
                                              TableRow(children: [
                                                buildButtonEnter()
                                              ]),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 15, left: 20),
                            color: Colors.white,
                            width: (((MediaQuery.of(context).size.width - 280) *
                                        0.8) -
                                    40) *
                                0.4,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "วิธีใช้งาน",
                                  style: TextStyle(
                                      color: Color(0xFF5D6AE0),
                                      fontSize: 28,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  "- ใส่จำนวนผู้เข้างาน",
                                  style: TextStyle(
                                      color: Color(0xFF666666), fontSize: 18),
                                ),
                                Text(
                                  "- ตรวจสอบยอดรวมที่ลูกค้าต้องจ่าย",
                                  style: TextStyle(
                                      color: Color(0xFF666666), fontSize: 18),
                                ),
                                Text(
                                  "- ใส่จำนวนเงินที่รับจากลูกค้า",
                                  style: TextStyle(
                                      color: Color(0xFF666666), fontSize: 18),
                                ),
                                Text(
                                  "- จากนั้นกดปุ่มยืนยัน",
                                  style: TextStyle(
                                      color: Color(0xFF666666), fontSize: 18),
                                ),
                                Text(
                                  "- ระบบแจ้งเงินทอน และ ปริ้นคูปองเข้างานให้กับลูกค้า",
                                  style: TextStyle(
                                      color: Color(0xFF666666), fontSize: 18),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  "* แจ้งปัญหาติดต่อ Line : @APPKIN",
                                  style: TextStyle(
                                      color: Color(0xFFFF5858), fontSize: 18),
                                ),
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  width: ((MediaQuery.of(context).size.width - 280) * 0.2) - 20,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0)),
                  ),
                  child: Column(
                    children: [
                      Text(
                        "จำนวนวันนี้",
                        style: TextStyle(
                            fontSize: 24,
                            color: Color(0xFF666666),
                            fontWeight: FontWeight.w600),
                      ),
                      IconButton(
                        iconSize: 30,
                        icon: new Image.asset(
                          'assets/images/group-user.png',
                          color: AppTheme.SEAHORSE_PRIMARY,
                        ),
                        tooltip: 'Closes application',
                        onPressed: () => {},
                      ),
                      Text(
                        numberFormat.format(peoplesCount),
                        style: TextStyle(
                            fontSize: 20,
                            color: Color(0xFF5D6AE0),
                            fontWeight: FontWeight.w600),
                      ),
                      Text(
                        "ท่าน",
                        style:
                            TextStyle(fontSize: 16, color: Color(0xFFB9B9B9)),
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildButton(
      String buttonText, double buttonHeight, Color buttonColor) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.1 * buttonHeight,
      decoration: BoxDecoration(
        color: Color(0xFF6A6D82),
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(10.0),
            bottomRight: Radius.circular(10.0),
            topLeft: Radius.circular(10.0),
            bottomLeft: Radius.circular(10.0)),
      ),
      child: FlatButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
              side: BorderSide(
                  color: Colors.white, width: 1, style: BorderStyle.solid)),
          padding: EdgeInsets.all(16.0),
          onPressed: () => {
            logger.i("buttonText" , buttonText),

            insertText(buttonText)
          },
          child: Text(
            buttonText,
            style: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.normal,
                color: Colors.white),
          )),
    );
  }

  Widget buildButtonDelete() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.1 * 1,
      decoration: BoxDecoration(
        color: Color(0xFFFF5858),
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(10.0),
            bottomRight: Radius.circular(10.0),
            topLeft: Radius.circular(10.0),
            bottomLeft: Radius.circular(10.0)),
      ),
      child: FlatButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0.0),
            side: BorderSide(
                color: Colors.white, width: 1, style: BorderStyle.solid)),
        padding: EdgeInsets.all(16.0),
        onPressed: () => {},
        child: IconButton(
          iconSize: 90,
          icon: new Image.asset(
            'assets/images/delete.png',
            color: AppTheme.SEAHORSE_WHITE_FF,
          ),
          tooltip: 'Closes application',
          onPressed: () => {},
        ),
      ),
    );
  }

  Widget buildButtonEnter() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.1 * 2,
      decoration: BoxDecoration(
        color: Color(0xFF5D6AE0),
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(10.0),
            bottomRight: Radius.circular(10.0),
            topLeft: Radius.circular(10.0),
            bottomLeft: Radius.circular(10.0)),
      ),
      child: FlatButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
              side: BorderSide(
                  color: Colors.white, width: 1, style: BorderStyle.solid)),
          padding: EdgeInsets.all(16.0),
          onPressed: () => {cashierCreate()},
          child: Align(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("ยืนยัน",
                    style: TextStyle(fontSize: 28, color: Color(0xFFFFFFFF))),
                IconButton(
                  iconSize: 90,
                  icon: new Image.asset(
                    'assets/images/enter.png',
                    color: AppTheme.SEAHORSE_WHITE_FF,
                  ),
                  tooltip: 'Closes application',
                  onPressed: () => {cashierCreate()},
                ),
              ],
            ),
          ))

    ,
    );
  }

  numberOfPeople(bool state) {
    setState(() {
      if (state)
        numberOfPeoples++;
      else {
        if (numberOfPeoples > 1) numberOfPeoples--;
      }
      peoples.text = "${numberOfPeoples}";
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    getConfig();
    getCount();
  }

  getCount() async {
    Response response =
        await Provider.of<ReportProvider>(context, listen: false)
            .getReport(context);

    if (response.statusCode == 200) {
      ReportModel model = ReportModel.fromJson(response.data);

      setState(() {
        peoplesCount = model.amount!;
      });
    }

    logger.i("getConfig", response.data);
  }

  getConfig() async {
    logger.i("getConfig", "getConfig");

    Response response = await Provider.of<HomeProvider>(context, listen: false)
        .getConfig(context);

    logger.i("getConfig", response.data.toString());

    pricePer1Use = 100;

    if (response.data['active'] == true) {
    } else if (response.data['status'] == true) {
      setState(() {
        for (int i = 0; i < response.data['price'].length; i++) {
          if (response.data['price'][i]['use']) {
            print(pricePer1Use);
            pricePer1Use = response.data['price'][i]['price'];
          }
        }
      });
    }

    logger.i("getConfig ${response.statusCode} ${response.data}");
  }

  Future<void> _showMyDialog(data) async {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertAlert(
            data: data,
          );
        });
  }

  cashierCreate() async {
    logger.i("cashierCreate", "Enter");

    int p = int.parse(peoples.text);

    int moneyIn = int.parse(moneyInCome.text);

    print(p);

    final jsons = jsonEncode({
      "amount": [
        {"amount": p, "_id": "1201851194044579-e31c-277f-9792-a8d35-69"}
      ],
      "cash": moneyIn
    });

    logger.i("cashierCreate ${jsons.toString()}");

    Response response = await Provider.of<HomeProvider>(context, listen: false)
        .cashierCreate(context, jsons);

    logger.i("cashierCreate ${response.statusCode} ${response.data}");

    if (response.data["status"]) {
      _showMyDialog(response.data);

      getCount();
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertInfo(
              title: "ผิดพลาด",
              detail: "${response.data['message']}",
            );
          });
    }
  }

  void _launchURL(_url) async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }

  checkNumber(String text) {
    print(text);

    if (text.toString().isEmpty || text.toString() == null) {
      numberOfPeoples = 0;
    } else {
      int numbers = int.parse(text);

      setState(() {
        numberOfPeoples = numbers;
      });
    }
  }

  insertText(String buttonText) {

    if(isMoneyInCome){

      print(buttonText);

      setState(() {
        moneyInCome.text = "dededed";

      });

    }
  }
}
