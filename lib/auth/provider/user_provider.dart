
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:seahorse/utility/config.dart';
import 'package:seahorse/utility/http_appkin.dart';

class UserProvider extends ChangeNotifier {

  Future<Response> login(BuildContext context, data) async {
    Response response = await HTTPAppKin(context)
        .post("${ConfigServer.serverHTTP}/emp/login", data: data);
    return response;
  }


}