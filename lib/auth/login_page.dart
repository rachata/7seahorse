import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seahorse/alert/alert_basic.dart';
import 'package:seahorse/auth/provider/user_provider.dart';
import 'package:seahorse/index.dart';
import 'package:seahorse/model/user.dart';
import 'package:seahorse/utility/app_theme.dart';

import '../main.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: AppTheme.SEAHORSE_WHITE_FF,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset("assets/images/logo.png"),
              SizedBox(height: 50,),
              Text("เข้าสู่ระบบพนักงาน"  , style: TextStyle(fontSize: 25 , fontStyle: FontStyle.normal , color: AppTheme.SEAHORSE_GREY_70),),

              Container(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                width:MediaQuery.of(context).size.width * 0.2 ,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    Container(
                      width: (MediaQuery.of(context).size.width * 0.2) * 0.1,
                      child: IconButton(

                        icon: new Image.asset('assets/images/user.png' , color: AppTheme.SEAHORSE_PRIMARY,),
                        tooltip: 'Closes application',
                        onPressed: () => {

                        },
                      ),
                    ),

                    Container(
                      width: (MediaQuery.of(context).size.width * 0.2) * 0.9,
                      child: TextField(
                        decoration: InputDecoration(

                          focusColor: Colors.red,
                            hoverColor: Colors.red,
                            border: OutlineInputBorder(

                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            filled: true,
                            hintStyle: TextStyle(color: Color(0xFF666666)),
                            hintText: "Username",
                            fillColor: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                width:MediaQuery.of(context).size.width * 0.2 ,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    Container(
                      width: (MediaQuery.of(context).size.width * 0.2) * 0.1,
                      child: IconButton(

                        icon: new Image.asset('assets/images/password.png' , color: AppTheme.SEAHORSE_PRIMARY,),
                        tooltip: 'Closes application',
                        onPressed: () => {

                        },
                      ),
                    ),

                    Container(
                      width: (MediaQuery.of(context).size.width * 0.2) * 0.9,
                      child: TextField(
                        decoration: InputDecoration(


                            border: OutlineInputBorder(

                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            filled: true,
                            hintStyle: TextStyle(color: Color(0xFF666666)),
                            hintText: "Password",
                            fillColor: Colors.white),
                      ),
                    )
                  ],
                ),
              ),


              SizedBox(
                  width: MediaQuery.of(context).size.width * 0.2,
                  height: 50,// <-- match_parent<-- match-parent
                  child: ElevatedButton(
                      child: Text(
                          "เข้าสู่ระบบ",
                          style: TextStyle(fontSize: 18 , color: AppTheme.SEAHORSE_WHITE_FD)
                      ),
                      style: ButtonStyle(
                          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor: MaterialStateProperty.all<Color>(AppTheme.SEAHORSE_PRIMARY),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  side: BorderSide(color: AppTheme.SEAHORSE_PRIMARY)
                              )
                          )
                      ),
                      onPressed: () => {
                        login()
                      }
                  )
              )
            ],
          ),
        )
      )

    );
  }

  login()async{


    // var result = await Process.run('open', ['https://osxdaily.com' , '--hide']);
    // print(result.stdout);



    final jsons = jsonEncode(
        {"username": "ninebatman", "password": "1150"});

    logger.i("Login ${jsons.toString()}");

    Response response = await Provider.of<UserProvider>(context, listen: false)
        .login(context, jsons);

    logger.i("login ${response.statusCode} ${response.data}");

    if (response.statusCode == 200) {
      if (response.data['success'] == false) {
        await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertInfo(
                title: "ผิดพลาด",
                detail: "${response.data['msg']}",
              );
            });
      } else {
        LoginUser loginUser = LoginUser.fromJson(response.data);

        if (loginUser.success == false) {


        } else {
          final body = json.encode(response.data);

          UserInfo userInfo = UserInfo();
          await userInfo.setUserInfo(body);


          LoginUser? user = await userInfo.getUserInfo();
          var bytes = utf8.encode(user!.user.id);

          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (_) => IndexPage()));
        }
      }
    }

  }
}
