import 'dart:async';

import 'package:flutter/material.dart';
import 'package:seahorse/appbar.dart';
import 'package:seahorse/model/SideBar.dart';
import 'package:seahorse/report/report.dart';
import 'package:seahorse/sidebar.dart';

import 'home/home.dart';
import 'main.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {

  late StreamSubscription subscription;



  Widget screen = HomePage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFEDEDED),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppBarMain(),
          Row(
            children: [
              SideBar(),
              Container(
                color: Color(0xFFEDEDED),
                width: MediaQuery.of(context).size.width - 280,
                height: MediaQuery.of(context).size.height - 74,
                child: screen,
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    subscription = eventBus.on<SideBarModel>().listen((event) { });
    subscription.onData(changeScreen);



  }

  changeScreen(data){

    print(data.action);

    setState(() {
      if(data.action == 1){

        screen = HomePage();
      }else{
        screen = ReportPage();
      }
    });
  }
}
