import 'package:after_layout/after_layout.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:seahorse/main.dart';
import 'package:seahorse/model/report.dart';
import 'package:seahorse/report/provider/report_provider.dart';

class ReportPage extends StatefulWidget {
  const ReportPage({Key? key}) : super(key: key);

  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage>
    with AfterLayoutMixin<ReportPage> {
  late ReportModel loginUser;
  int numberOfPeople = 0;
  double amount = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width - 280,
        height: MediaQuery.of(context).size.height - 74,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0),
              topLeft: Radius.circular(10.0),
              bottomLeft: Radius.circular(10.0)),
        ),
        child: Consumer<ReportProvider>(builder: (context, providerData, _) {
          return FutureBuilder(
              future: providerData.getReport(context),
              builder: (context, AsyncSnapshot<Response> snapshot) {
                if (snapshot.hasData) {
                  loginUser = ReportModel.fromJson(snapshot.data!.data);

                  var w = MediaQuery.of(context).size.width - 280 - 50;

                  return Scaffold(

                    appBar: AppBar(
                      backgroundColor: Colors.white,
                      elevation: 0,
                      title:  Container(
                          alignment: Alignment.center,
                          height: 70,
                          decoration: BoxDecoration(
                            color: Color(0xFFFFFFFF),
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0),
                                topLeft: Radius.circular(10.0),
                                bottomLeft: Radius.circular(10.0)),
                          ),
                          child: ListTile(
                            leading: Container(
                              width: (MediaQuery.of(context).size.width -
                                  280 -
                                  50) *
                                  0.15,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "เวลา",
                                  style: TextStyle(
                                      color: Color(0xFF6A6D82), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            title: Container(
                              width: (MediaQuery.of(context).size.width -
                                  280 -
                                  50) *
                                  0.2,
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "จำนวนเงิน",
                                  style: TextStyle(
                                      color: Color(0xFF6A6D82), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            trailing: Container(
                              width: (MediaQuery.of(context).size.width -
                                  280 -
                                  50) *
                                  0.2,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "จำนวนคน",
                                  style: TextStyle(
                                      color: Color(0xFF6A6D82), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                          )),
                    ),
                    backgroundColor: Colors.white,
                    body: ListView.builder(
                        itemCount: loginUser.data!.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ListTile(
                            leading: Container(
                              width: w * 0.15,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "${loginUser.data![index].title}",
                                  style: TextStyle(
                                      color: Color(0xFFB9B9B9), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            title: Container(
                              width: w * 0.2,
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "${numberFormat.format (loginUser.data![index].total)}",
                                  style: TextStyle(
                                      color: Color(0xFFB9B9B9), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            trailing: Container(
                              width: w * 0.2,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "${numberFormat.format(loginUser.data![index].amount)}",
                                  style: TextStyle(
                                      color: Color(0xFFB9B9B9), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                          );
                        }),
                    persistentFooterButtons: [
                      Container(
                          alignment: Alignment.bottomCenter,
                          height: 70,
                          decoration: BoxDecoration(
                            color: Color(0xFF5D6AE0),
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0),
                                topLeft: Radius.circular(10.0),
                                bottomLeft: Radius.circular(10.0)),
                          ),
                          child: ListTile(
                            leading: Container(
                              width: (MediaQuery.of(context).size.width -
                                      280 -
                                      50) *
                                  0.15,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "รวม",
                                  style: TextStyle(
                                      color: Color(0xFFFFFFFF), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            title: Container(
                              width: (MediaQuery.of(context).size.width -
                                      280 -
                                      50) *
                                  0.2,
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "${numberFormat.format(loginUser.total)}",
                                  style: TextStyle(
                                      color: Color(0xFFFFFFFF), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            trailing: Container(
                              width: (MediaQuery.of(context).size.width -
                                      280 -
                                      50) *
                                  0.2,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "${numberFormat.format(loginUser.amount)}",
                                  style: TextStyle(
                                      color: Color(0xFFFFFFFF), fontSize: 24),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                          ))
                    ],
                  );
                } else {
                  return Center(
                    child: Text(
                      "รอสักครู่",
                      style: TextStyle(fontSize: 20),
                    ),
                  );
                }
              });
        }));
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // getReport();
  }

  getReport() async {
    Response response =
        await Provider.of<ReportProvider>(context, listen: false)
            .getReport(context);

    if (response.statusCode == 200) {
      if (loginUser.status == true) {
        setState(() {
          loginUser = ReportModel.fromJson(response.data);
          numberOfPeople = loginUser.total!;
          amount = loginUser.amount!.toDouble();
        });
      }
    }
    print(response);
  }
}
