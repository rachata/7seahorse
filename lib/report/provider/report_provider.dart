
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:seahorse/utility/config.dart';
import 'package:seahorse/utility/http_appkin.dart';

class ReportProvider extends ChangeNotifier {

  Future<Response> getReport(BuildContext context) async {
    Response response = await HTTPAppKinMain(context)
        .get("${ConfigServer.serverHTTPMain}/ticket/report/dailyCus");
    return response;
  }


}