import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:seahorse/utility/app_theme.dart';
import 'package:intl/intl.dart';
class AppBarMain extends StatefulWidget {
  const AppBarMain({Key? key}) : super(key: key);

  @override
  _AppBarMainState createState() => _AppBarMainState();
}

class _AppBarMainState extends State<AppBarMain> {

  String _timeString = "";


  @override
  void initState() {

    _timeString = _formatDateTime(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 74,
      color: Colors.white,
      padding: EdgeInsets.all(15),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset("assets/images/logo.png"),
          Row(

            children: [
              IconButton(
                icon: new Image.asset('assets/images/time.png' , color: AppTheme.SEAHORSE_PRIMARY,),
                tooltip: 'Closes application',
                onPressed: () => {

                },
              ),
              Text(_timeString , style: TextStyle(fontSize: 20 ,color: Color(0xFF6A6D82) ),)

            ],
          )

        ],
      ),
    );
  }


  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    setState(() {
      _timeString = formattedDateTime;
    });
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('dd/MM/yyyy HH:mm').format(dateTime);
  }

}
