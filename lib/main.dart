import 'dart:io';

import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:seahorse/auth/login_page.dart';
import 'package:seahorse/report/provider/report_provider.dart';
import 'package:seahorse/utility/app_theme.dart';
import 'auth/provider/user_provider.dart';
import 'home/provider/home_provider.dart';
import 'index.dart';
import 'package:desktop_window/desktop_window.dart';


final numberFormat = new NumberFormat("#,###", "en_US");

var logger = Logger(
  filter: null, // Use the default LogFilter (-> only log in debug mode)
  printer: PrettyPrinter(), // Use the PrettyPrinter to format and print log
  output: null,
);

EventBus eventBus = new EventBus();

void main() async {
  HttpOverrides.global = MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => UserProvider()),
          ChangeNotifierProvider(create: (_) => HomeProvider()),
          ChangeNotifierProvider(create: (_) => ReportProvider()),
        ],
        child: MaterialApp(
            title: '7SeaHorse',
            theme: ThemeData(
                primarySwatch: AppTheme.SEAHORSE_PRIMARY_MAT,
                fontFamily: AppTheme.fontName),
            home: LoginPage()));
  }
}

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

